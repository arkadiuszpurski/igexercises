package exercises.ex3;

/**
   As an input to this program, you provide word
   As a result it is expected that all permutations of letters in this word will be printed out as well as number of permutations
   Optional : verify if each permutation of a word is a valid word in https://sjp.pl/ dictionary

   Use Permutator class
   Add unit tests to validate if the program works properly
 */
public class Permutations {

   private Permutator permutator;


   public String printPermutations(String input){
         Permutator input1 = new Permutator(input);
         String str = input1.getInput();
         int n = str.length();
         Permutations permutation = new Permutations();
         permutation.permute(str, 0, n-1);
         return str;
      }

      private void permute(String str, int l, int r)
      {
         if (l == r)
            System.out.println(str);
         else
         {
            for (int i = l; i <= r; i++)
            {
               str = swap(str,l,i);
               permute(str, l+1, r);
               str = swap(str,l,i);
            }
         }
      }

      public String swap(String a, int i, int j)
      {
         char temp;
         char[] charArray = a.toCharArray();
         temp = charArray[i] ;
         charArray[i] = charArray[j];
         charArray[j] = temp;
         return String.valueOf(charArray);
      }


   }

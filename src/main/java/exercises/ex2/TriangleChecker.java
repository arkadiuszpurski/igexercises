package exercises.ex2;

/**
 * The goal of this exercise is to implement a method below which checks triangle type out of given sides: int a, int b, int c
 * or throws NotATriangleException.
 * You should also implement unit tests verifying this method
 */
public class TriangleChecker {

    private int a;
    private int b;
    private int c;

    public TriangleChecker(int a, int b, int c){
        this.a = a;
        this.b = b;
        this.c = c;
    }

    public Triangle checkTriangleType() throws NotATriangleException {

        if (!isTriangle(a, b, c)) {
            throw new NotATriangleException();
        }
        if ( a == b && b == c) {
            return Triangle.EQUILATERAL;
        }
        if (b == c || a == b || c == a) {
            return Triangle.ISOSCELES;
        }
        return Triangle.OTHER;
    }

    private static boolean isTriangle(int a, int b, int c) {
        // positive lengths, and sides that can meet
        return a > 0 && b > 0 && c > 0  && (a + b) > c && (a + c) > b && (b + c) > a;
    }
}




package exercises.ex2;

import org.junit.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;

//import org.junit.jupiter.api.Test;

/**
 * unit tests for TriangleChecker should be implemented here
 */
public class TriangleCheckerTest {


    @Test(expected = NotATriangleException.class)
    public void testInvalidTriangleWithNegativeSide() throws NotATriangleException {
        new TriangleChecker(21,0,22).checkTriangleType();
    }

    @Test
    public void testEquilateral() throws NotATriangleException {
        assertEquals(Triangle.EQUILATERAL, new TriangleChecker(3, 3, 3).checkTriangleType());
        assertEquals(Triangle.EQUILATERAL, new TriangleChecker(2, 2, 2).checkTriangleType());
        assertEquals(Triangle.EQUILATERAL, new TriangleChecker(5, 5, 5).checkTriangleType());
    }

    @Test
    public void testIsosceles() throws NotATriangleException {
        assertEquals(Triangle.ISOSCELES,  new TriangleChecker(3, 3, 2).checkTriangleType());
        assertEquals(Triangle.ISOSCELES,  new TriangleChecker(3, 2, 3).checkTriangleType());
    }
    @Test
    public void testOther() throws NotATriangleException {
        assertEquals(Triangle.OTHER, new TriangleChecker(21,20,22).checkTriangleType());
    }


}
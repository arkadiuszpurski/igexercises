package exercises.ex1;

import org.hamcrest.core.IsEqual;
import org.junit.Test;
import io.restassured.RestAssured.*;
import io.restassured.matcher.RestAssuredMatchers.*;

import static io.restassured.RestAssured.get;
//import  org.hamcrest.Matchers.equalTo;
import static org.assertj.core.api.Assertions.*;
import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.core.StringContains.containsString;

/**
 * The purpose of this test is to check that unauthorized user is not able to get pending agreements.
 *
 * 1. Send GET request without headers to https://api.ig.com/usermanagement/pendingagreements. Please use REST-assured (http://rest-assured.io) to do it.
 * 2. Check if:
 *    - response code is 401
 *    - global error message is: "error.security.client-token-missing"
 * 
 * You can additinally use these libraries: Junit, TestNg, AssertJ  
 */
public class RestCallTest  {
    @Test
    public void givenUrl_with401status() {
        get("https://api.ig.com/usermanagement/pendingagreements").then().statusCode(401).assertThat()
                //.body("globalErrors")containsString("Acme garage"));
    .body("globalErrors",equalTo("[error.security.client-token-missing]"));
    }
}
